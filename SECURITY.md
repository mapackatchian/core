# Security

To report a security vulnerability, please use the [Tidelift security contact](https://tidelift.com/security).
Tidelift will then coordinate the fix and disclosure.
